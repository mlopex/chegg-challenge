import React, { Component } from 'react';
import './IssueList.css';
import {connect} from "react-redux";
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { setIssueOrder } from "../../actions/issuesActions";
import { reorderList } from "../../lib/arrayUtils";
import IssueItem from "../IssueItem/IssueItem";

class IssueList extends Component {

  issueListItems = (allIssues) => {

    return allIssues.map((item,index) => {
      return (
        <Draggable key={item.id} draggableId={""+item.id} index={index}>
          {(provided, snapshot) => (
            <div className={'issue-item-container'}
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
            >
              <IssueItem issue={item}/>
            </div>
            )}
        </Draggable>
      );
    });

  };

  issueListContent = (issues) => {
    if(issues.loading) {
      return(<div className={"issue-list-message"}>Loading...</div>);
    } else if(!issues.data || issues.data.length===0) {
      return(<div className={"issue-list-message"}>No Issues. Hooray!</div>);
    } else {
      return (
        <DragDropContext onDragEnd={this.onDragEnd}>
          <Droppable droppableId="droppable">
            {(provided, snapshot) => (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
              >
                {this.issueListItems(issues.data)}
                {provided.placeholder}
              </div>
            )}
          </Droppable>
        </DragDropContext>
      )
    }
  };

  onDragEnd = (result) => {

    const { issues: { data }, onIssueReorder } = this.props;

    // if dropped outside droppable region, cancel
    if (!result.destination) {
      return;
    }

    // determine new list order
    const newOrder = reorderList(
      data,
      result.source.index,
      result.destination.index
    );

    // dispatch action with new order
    onIssueReorder(newOrder);

  };

  render() {
    const { issues } = this.props;
    return (
        <div className={"IssueList"}>
          {this.issueListContent(issues)}
        </div>
    )
  }
}

// extract props from Redux store
const mapStateToProps = (state) => {
  return {
    issues: state.issues
  }
};

// map callbacks to Redux action creators
const mapDispatchToProps = dispatch => ({
  onIssueReorder(newOrder) {
    dispatch(
      setIssueOrder(newOrder)
    );
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(IssueList);