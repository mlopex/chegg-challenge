import React, { Component } from 'react';
import './RepoList.css';
import {connect} from "react-redux";
import {setSelectedRepoId} from "../../actions/reposActions";

class RepoList extends Component {

  repoListItems = (allRepos) => {
    const { repos } = this.props;
    return allRepos.map(item => {
      if(repos.selectedId===item.id) {
        return <div key={item.id} className={'repo-item selected'}>{item.fullName}</div>
      } else {
        return <div key={item.id} className={'repo-item clickable'} onClick={(e) => this.handleRepoClick(item.id)}>{item.fullName}</div>
      }
    });

  };

  repoListContent = (repos) => {
    if(repos.loading) {
      return(<div>Loading...</div>);
    } else if(!repos.data || repos.data.length===0) {
      return(<div>No Repos.</div>);
    } else {
      return(
        <div>
          {this.repoListItems(repos.data)}
        </div>
      )
    }
  };

  handleRepoClick = (id) => {
    const { onRepoSelected } = this.props;
    onRepoSelected(id);
  };

  render() {
    const { repos } = this.props;
    return (
      <div className={"RepoList"}>
        {this.repoListContent(repos)}
      </div>
    )
  }

}

// extract props from Redux store
const mapStateToProps = (state) => {
  return {
    repos: state.repos
  }
};

// map callbacks to Redux action creators
const mapDispatchToProps = dispatch => ({
  onRepoSelected(id) {
    dispatch(
      setSelectedRepoId(id)
    );
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(RepoList);