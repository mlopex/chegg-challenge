import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux';
import { store } from '../../store'
import PageContainer from '../PageContainer/PageContainer'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
            <PageContainer/>
        </div>
      </Provider>
    );
  }

}

export default App;
