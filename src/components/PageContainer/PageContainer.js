import React, { Component } from 'react';
import './PageContainer.css';
import { connect } from "react-redux";
import LoginForm from '../LoginForm/LoginForm';
import RepoList from '../RepoList/RepoList';
import IssueList from "../IssueList/IssueList";
import { gitLogout } from "../../actions/gitProfileActions";

class PageContainer extends Component {

  handleLogoutClick = () => {
    const { onLogout } = this.props;
    onLogout();
  };

  render() {

    const { gitProfile, repos } = this.props;

    if(!gitProfile.apiKey) {
      return(<LoginForm />)
    } else {
      return(
        <div className={"PageContainer"}>
          <div className={"page-column-container"}>
            <div className={"page-left-column"}>
              <div className={"column-title"}>Repos:</div>
              <RepoList />
            </div>
            {repos.selectedId!==null &&
            <div className={"page-right-column"}>
              <div className={"column-title"}>Issues:</div>
              <IssueList />
            </div>
            }
          </div>
          <button id={"logout-button"} onClick={this.handleLogoutClick}>Logout</button>
        </div>
      );
    }

  }
}

// extract props from Redux store
const mapStateToProps = (state) => {
  return {
    gitProfile: state.gitProfile,
    repos: state.repos
  }
};

// map callbacks to Redux action creators
const mapDispatchToProps = dispatch => ({
  onLogout() {
    dispatch(
      gitLogout()
    );
  }
});

export default connect(mapStateToProps,mapDispatchToProps)(PageContainer);