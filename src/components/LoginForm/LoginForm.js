import React, { Component } from 'react';
import './LoginForm.css';
import {connect} from "react-redux";
import { gitAPIKeySaved } from "../../actions/gitProfileActions";

class LoginForm extends Component {

  constructor(props) {
    super(props);

    const { gitProfile } = this.props;

    this.state = {
      currentKey: gitProfile.apiKey
    };

  }

  handleSaveButtonClick = (event) => {
    const { onSaveAPIKey } = this.props;

    onSaveAPIKey(this.state.currentKey);
  };

  handleAPIKeyChange = (event) => {

    const newVal = event.target.value;

    this.setState(state => ({
      currentKey: newVal
    }));

  };

  render() {
    const { gitProfile } = this.props;
    return (
      <div className={"LoginForm"}>
        <input onChange={this.handleAPIKeyChange} placeholder={"Paste GitHub API Key Here"} value={this.state.currentKey}/>
        <button onClick={this.handleSaveButtonClick}>Save</button>
        <span className={"status-message"}>{gitProfile.statusMessage}</span>
      </div>
    )
  }

}

// extract props from Redux store
const mapStateToProps = (state) => {
  return {
    gitProfile: state.gitProfile
  }
};

// map callbacks to Redux action creators
const mapDispatchToProps = dispatch => ({
  onSaveAPIKey(newAPIKey) {
    dispatch(
      gitAPIKeySaved(newAPIKey)
    );
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);