import React from "react";
import IssueItem from "./IssueItem";
import renderer from "react-test-renderer";
import { issue } from "../../lib/mockData";

it("IssueItem matches snapshot", () => {

  const tree = renderer.create(
    <IssueItem issue={issue}/>
  );

  expect(tree).toMatchSnapshot();

});