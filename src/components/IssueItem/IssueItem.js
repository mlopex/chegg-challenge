import React, { Component } from 'react';
import './IssueItem.css';
import moment from 'moment';

class IssueItem extends Component {
  render() {

    const {
      issue: {
        title,
        createdAt,
        updatedAt,
        user: {
          avatarUrl
        }
      }
    } = this.props;

    const createdAtDate = new Date(createdAt);
    const updatedAtDate = new Date(updatedAt);

    const createdAtFormatted = moment(createdAtDate).format("DD/MM/YYYY")
    const updatedAtFormatted = moment(updatedAtDate).fromNow();

    return (
      <div className={"IssueItem"}>
        <div className={"issue-item-row"}>
          <div className={"issue-item-element"} style={{flex:"40px"}}>
            <img src={avatarUrl} width={40} height={40}/>
          </div>
          <div className={"issue-item-element"} style={{flex:"100%"}}>
            {title}
          </div>
        </div>
        <div className={"issue-item-row"}>
          <div className={"issue-item-element detail"} style={{flex:"50%", textAlign:"right"}}>
            Created: {createdAtFormatted}
          </div>
          <div className={"issue-item-element detail"} style={{flex:"50%", textAlign:"right"}}>
            Updated: {updatedAtFormatted}
          </div>
        </div>
      </div>
    )
  }

}

export default IssueItem;