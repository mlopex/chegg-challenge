const initialState = {
  gitProfile: {
    apiKey: "",
    statusMessage: ""
  },
  repos: {
    loading: false,
    selectedId: null,
    data: []
  },
  issues: {
    loading: false,
    data: []
  }
};

export default initialState;