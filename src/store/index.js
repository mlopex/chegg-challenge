import {applyMiddleware, compose, createStore} from "redux";
import thunk from 'redux-thunk';
import * as lodash from 'lodash';
import reducers from "../reducers";
import initialState from "./initialState";
import {loadState, saveState} from "../lib/localStorage";

// apply middleware
const enhancers = compose(
  applyMiddleware(
    thunk
  )
);

// load persisted state from localStorage
const persistedState = loadState();

// create store with persisted state if available
export const store = createStore(
  reducers,
  persistedState || initialState,
  enhancers
);

// persist the state to localStorage at most once every 500ms
store.subscribe(lodash.throttle(() => {
  saveState(store.getState());
}, 500));