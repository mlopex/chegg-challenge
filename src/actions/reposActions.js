import * as TYPES from "./actionTypes";
import {setIssuesLoading,gotIssues} from "./issuesActions";
import Octokat from 'octokat';
import {getSelectedRepo} from "../selectors";

// action for successful fetch of repo data
export function gotRepos( repos ) {
  return {
    type: TYPES.GOT_REPOS,
    payload: {
      repos: repos
    }
  };
}

// action to set loading flag on repos
export function setReposLoading( loading ) {
  return {
    type: TYPES.SET_REPOS_LOADING,
    payload: {
      loading: loading
    }
  };
}

// action to set selected repo ID
export function setSelectedRepoId( id ) {

  // return a thunk
  return((dispatch,getState) => {

    // first, dispatch SET_SELECTED_REPO_ID action (this action) with the selected ID
    dispatch({
      type: TYPES.SET_SELECTED_REPO_ID,
      payload: {
        id: id
      }
    });

    // then, dispatch SET_ISSUES_LOADING action to display loading graphic
    dispatch(setIssuesLoading(true));

    // get selected repo details
    const repo = getSelectedRepo(getState());

    // then, fetch the repo's issues
    const gh = new Octokat({token: '37c3f1041847f02987c52d2b753389403b89f124'});
    gh.fromUrl(`https://api.github.com/repos/${repo.fullName}/issues`).fetch()
      .then(resp => {
        const newIssues = resp.items;
        dispatch(gotIssues(newIssues));
      })
      .catch(err => {
        console.log(err);
      });

  });

}