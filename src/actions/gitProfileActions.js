import * as TYPES from "./actionTypes";
import { gotRepos, setReposLoading } from "./reposActions";
import Octokat from 'octokat';

// action for API key being saved from the GUI
export function gitAPIKeySaved( newKey ) {

  // return a thunk
  return((dispatch,getState) => {

    // first, dispatch GIT_API_KEY_SAVED action (this action) with the new key
    dispatch({
      type: TYPES.GIT_API_KEY_SAVED,
      payload: {
        newKey: newKey
      }
    });

    // then, dispatch SET_REPOS_LOADING action to display loading graphic
    dispatch(setReposLoading(true));

    // then, fetch the user's repos
    const gh = new Octokat({token: newKey});
    gh.fromUrl('https://api.github.com/user/repos').fetch()
      .then(resp => {
        const newRepos = resp.items;
        dispatch(gotRepos(newRepos));
      })
      .catch(err => {
        console.log(err);
        dispatch(gitLoginFailed(err.message));
      });

  });

};

// action to clear the saved git API key
export function gitLogout() {
  return {
    type: TYPES.GIT_LOGOUT,
    payload: {}
  };
}

// action to update the git login status on error
export function gitLoginFailed(message) {
  return {
    type: TYPES.GIT_LOGIN_FAILED,
    payload: {
      message: message
    }
  };
}