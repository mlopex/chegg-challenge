import * as TYPES from "./actionTypes";

// action for successful fetch of issues data
export function gotIssues( issues ) {
  return {
    type: TYPES.GOT_ISSUES,
    payload: {
      issues: issues
    }
  };
}

// action to set loading flag on issues
export function setIssuesLoading( loading ) {
  return {
    type: TYPES.SET_ISSUES_LOADING,
    payload: {
      loading: loading
    }
  };
}

// action to set issue order
export function setIssueOrder( issues ) {
  return {
    type: TYPES.SET_ISSUE_ORDER,
    payload: {
      issues: issues
    }
  };
}