
// get all details of selected repo
// TODO: keep repos in an id-indexed opbject so that this becomes O(1)
export const getSelectedRepo = (state) => {
  for(let repo of state.repos.data) {
    if(repo.id === state.repos.selectedId) {
      return repo;
    }
  }
  return {};
};