import {combineReducers} from 'redux';
import gitProfileReducer from "./gitProfileReducer";
import reposReducer from "./reposReducer";
import issuesReducer from "./issuesReducer";

const rootReducer = combineReducers({
  gitProfile: gitProfileReducer,
  repos: reposReducer,
  issues: issuesReducer
});

export default rootReducer;