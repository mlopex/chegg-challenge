import issuesReducer from './issuesReducer';
import { gitLogout } from "../actions/gitProfileActions";

it("should reset issue state on logout", () => {

  const initialState = {
    loading: true,
    data: [1,2,3,4]
  };

  const action = gitLogout();

  const newState = issuesReducer(initialState, action);

  expect(newState.loading).toBeFalsy();
  expect(newState.data.length).toEqual(0);

});