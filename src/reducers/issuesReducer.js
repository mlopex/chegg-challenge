import * as TYPES from '../actions/actionTypes';

function issuesReducer(state = {}, action) {

  switch (action.type) {

    case TYPES.GOT_ISSUES:
      return {
        ...state,
        loading: false,
        data: action.payload.issues
      };

    case TYPES.SET_ISSUES_LOADING:
      return {
        ...state,
        loading: action.payload.loading
      };

    case TYPES.SET_ISSUE_ORDER:
      return {
        ...state,
        data: action.payload.issues
      };

    case TYPES.GIT_LOGOUT:
      return {
        ...state,
        loading: false,
        data: []
      };

    default:
      return state;

  }
}

export default issuesReducer;