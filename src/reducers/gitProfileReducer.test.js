import gitProfileReducer from './gitProfileReducer';
import { gitLogout } from "../actions/gitProfileActions";

it("should reset Git profile on logout", () => {

  const initialState = {
    apiKey: "ABC",
    statusMessage: "some status message"
  };

  const action = gitLogout();

  const newState = gitProfileReducer(initialState, action);

  expect(newState.apiKey).toEqual("");
  expect(newState.statusMessage).toEqual("");

});