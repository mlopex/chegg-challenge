import * as TYPES from '../actions/actionTypes';

function gitProfileReducer(state = {}, action) {

  switch (action.type) {

    case TYPES.GIT_API_KEY_SAVED:
      return {
        ...state,
        apiKey: action.payload.newKey,
        statusMessage: ""
      };

    case TYPES.GIT_LOGOUT:
      return {
        ...state,
        apiKey: "",
        statusMessage: ""
      };

    case TYPES.GIT_LOGIN_FAILED:
      return {
        ...state,
        apiKey: "",
        statusMessage: action.payload.message
      };

    default:
      return state;

  }
}

export default gitProfileReducer;