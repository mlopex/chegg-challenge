import reposReducer from './reposReducer';
import { gitLogout } from "../actions/gitProfileActions";

it("should reset issue state on logout", () => {

  const initialState = {
    loading: true,
    selectedId: 1,
    data: [1,2,3,4]
  };

  const action = gitLogout();

  const newState = reposReducer(initialState, action);

  expect(newState.loading).toBeFalsy();
  expect(newState.data.length).toEqual(0);
  expect(newState.selectedId).toEqual(null);

});