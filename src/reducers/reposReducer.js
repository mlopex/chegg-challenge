import * as TYPES from '../actions/actionTypes';

function reposReducer(state = {}, action) {

  switch (action.type) {

    case TYPES.GOT_REPOS:
      return {
        ...state,
        loading: false,
        data: action.payload.repos
      };

    case TYPES.SET_REPOS_LOADING:
      return {
        ...state,
        loading: action.payload.loading
      };

    case TYPES.SET_SELECTED_REPO_ID:
      return {
        ...state,
        selectedId: action.payload.id
      };

    case TYPES.GIT_LOGOUT:
      return {
        ...state,
        loading: false,
        selectedId: null,
        data: []
      };

    default:
      return state;

  }
}

export default reposReducer;