export const issue = {
  title: "Mock Issue Title",
  createdAt:  "2019-01-10T12:30:00.000Z",
  updatedAt: "2019-07-15T20:26:30.000Z",
  user: {
    avatarUrl: "https://avatars1.githubusercontent.com/u/1876279?v=4"
  }
};