This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Chegg Coding Challenge

This is a simple React/Redux demo application that:

- requests a GitHub API Key from the user
- if valid, displays a vertical list of all the repos on GitHub associated with that user
- when any repo is selected, all issues associated with that repo are displayed
- the user can then drag and drop the list of issues to change the order
- the current API key, repos and issue order is persisted to localStorage on every change
- a 'Logout' button is provided to clear out the application's state and/or enter a new API key
- if window width < 750px, the issue display changes to a two-line format for readability


## Future Improvements / TODO's:

- look and feel!
- tests for all components
- refactor the Redux store by storing all repos and issues in an object keyed by ID instead of an array (this makes for O(1) lookups throughout the application and helps ensure that the client-side code scales)
- implement more selectors and use in all mapStateToProps lookups, allowing for more efficient refactoring of Redux store
- clean up data returned from API before putting it in Redux store (for example the output from the GitHub API library used contains functions, which are not serializable and generally not great to put in Redux)
- implement better error handling and display nicer messages on error conditions, such as the user entering and invalid API key or a failed fetch
- header showing logged in user
- ability to filter repos/issues (only open issues, etc.)

## Installation

`npm install`

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
